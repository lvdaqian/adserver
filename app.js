var express = require('express');
var app = express();
var fs= require('fs') 
var logger = require('morgan')
var router = require('./router')
var ad = require('./ad')
var cookieParser = require('cookie-parser')
var api = require('./api')

// var router = app.Router();
app.set('port', (process.env.PORT || 3001));

logger.token('headers', function(req, res){ return JSON.stringify(req.headers); })

// create a rotating write stream 

app.use(cookieParser())

app.use(logger('[:date[iso]] ip :remote-addr - user :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":headers"'));

app.use('/static/ad(40|41|51|60)?.html', router)
//app.use('/static/mobile/ad(1)?.html', router)

app.use('/static', express.static(__dirname + '/static'));
app.use('/api',api)


app.get('*', function(req, res) {
    console.log('can not find path',req.path)
	res.status(404).end();
})

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}
