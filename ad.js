var router = require('express').Router();



router.get('/:redirectUrl/confirm.js',function(req, res) {

  var base64String = req.param('redirectUrl')
  var redirectUrl = new Buffer(base64String,'base64').toString()
  res.end(redirectUrl);
})

module.exports = router;
