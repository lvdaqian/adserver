var fs= require('fs') 
var readline = require('readline')
var url = require('url')
var router = require('express').Router();
var index = 0
var targetList = {};

var targetDir = process.env.adtarget || ''
console.log(targetDir)
fs.readdirSync(__dirname + '/target/' + targetDir).forEach(function(file) {
  var filename = __dirname + '/target/' + targetDir + '/' + file;
    console.log(filename)

    var stat = fs.lstatSync(filename)

    if (stat.isDirectory()) {
      return
    };

  var rl = readline.createInterface({
    input: fs.createReadStream(filename)
  });

  var target = {}
  target.urls = []

  targetList[file] = target

  rl.on('line', function (line) {
    target.urls.push(url.parse(line, true))
  });

  rl.on('close', function () {
    target.max = target.urls.length
  });
  
});

var djb2Code = function(str){
    var hash = 5381;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) + hash) + char; /* hash * 33 + c */
    }
    return hash;
}

function getEid(uaString)
{
  var ua = uaString.toLowerCase();
  var now = new Date();
  var tag = 'M'+now.getMonth()+'D'+now.getDate();
  if(ua.indexOf("msie") > -1 && ua.indexOf("opera") == -1)
  {
    tag +='P1V'+ (ua.match(/(msie|firefox|webkit|opera)[\/:\s](\d+)/) ? RegExp.$2 : "0")
  }
  else if(ua.indexOf("gecko") > -1 && ua.indexOf("khtml") == -1)
  {
    tag +='P2V'+(ua.match(/(msie|firefox|webkit|opera)[\/:\s](\d+)/) ? RegExp.$2 : "0")
  }
  else if(ua.indexOf("applewebkit") > -1)
  {
    tag +='P3V'+(ua.match(/(msie|firefox|webkit|opera)[\/:\s](\d+)/) ? RegExp.$2 : "0")
  }
  else if(ua.indexOf("opera") > -1)
  {
    tag +='P4V'+(ua.match(/(msie|firefox|webkit|opera)[\/:\s](\d+)/) ? RegExp.$2 : "0")
  }
  return tag
}

function getLinks (url, cnzzid) {
   var target = targetList[url];
   if (target == undefined ) {
     return undefined;
   }
   var index = Math.abs(djb2Code(cnzzid))
   var urlObject = target.urls[ index % target.max];

   return urlObject;
}

router.use(function(req, res, next) {
  console.log('enter router')
  var ipString = req.ip + req.ips.join(' ');
  var pathString = req.originalUrl.split('?')[0];
  if (ipString.match('60.191.70.') || ipString.match('180.173.23.') || ipString.match('180.173.224.')) {
  	console.log('unsafe ipString:' + ipString)
  	next();
  	return;
  };

  console.log('safe ip:' + ipString, pathString)
  var webID = '1256656887';
  var TargetTag = 't'
  if (pathString == '/static/ad40.html') {
      webID = '1256747957'
      TargetTag += '40'
  };
  if (pathString == '/static/ad41.html') {
      webID = '1257016593'
      TargetTag += '41'
  };
  if (pathString == '/static/ad51.html') {
      webID = '1256862028'
      TargetTag += '51'
  };  
  if (pathString == '/static/ad60.html') {
      webID = '1256862042'
      TargetTag += '60'
  };
  if (pathString == '/static/mobile/ad.html') {
      webID = '1257406313'
      TargetTag += 'mobile'
  };

  if (pathString == '/static/mobile/ad1.html') {
      webID = '1257620290'
      TargetTag += 'mobile'
  };

  console.log('webID:', webID)

  var cnzzid = req.cookies.CNZZDATA1256656887 || ipString
  var reffer = req.get('Referrer')
  if (reffer === undefined) { next(); return;};
  console.log('reffer:',reffer,cnzzid)

  var addr = url.parse(reffer, true)

  var hostname = addr.hostname;
  var tagIndex = hostname.indexOf('.')
  var hostTag = hostname.substring(0,tagIndex)

  var mainDomain = hostname.substring(tagIndex+1)

  if (hostTag == 'm') {
    mainDomain = hostname;
  };

  console.log('mainDomain',mainDomain)
  var link = getLinks(mainDomain,cnzzid);

  if (link === undefined) { next(); return; };
  
  var num = Math.abs(djb2Code(cnzzid));
  var adid = 'ad' + num;
  num = num%100;

  // clear default value
  addr.search = ''; 
  link.search = '';
  delete link.query.t
  link.query.e = hostTag + getEid(req.get('User-Agent')) + 'U' + num + TargetTag

  // need new filter here
  // for jd 
  if (addr.query.jd_pop !== undefined) { next(); return;};
  if (addr.query.utm_source !== undefined)
  {   
     delete addr.query.utm_source;
     delete addr.query.utm_medium;
     delete addr.query.utm_campaign;
     delete addr.query.utm_term;
     link.query.e += 'Su'
  }

  // for yhd
  if (addr.query.tracker_u !== undefined) 
  {
     delete addr.query.tracker_u;
     delete addr.query.tracker_type;
     delete addr.query.website_id;
     delete addr.query.uid;
     link.query.e += 'Sy'
  };
  link.query.t = '';
  var to = url.format(addr)
  var toUrl = url.format(link)+to;
  console.log(toUrl)
  res.end('<html><head></head><body style="display:none"><script src="http://s11.cnzz.com/z_stat.php?id='+webID+'&web_id='+webID+'" language="JavaScript"></script>'
    +'<script> location.href ="'+ toUrl
    +'";</script></div></body></html>'
  );
})

module.exports = router;
